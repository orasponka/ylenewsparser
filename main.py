import feedparser
import os
import readline

commands = [
    "help - displays this help message",
    "clear - clears screen",
    "exit - exit this program",
    "version - prints version",
    "titles - prints the titles and indexes of all articles",
    "article 'index' - prints more info about specific article",
    "feeds - prints all available feeds",
    "setfeed 'feed' - selects feed for use",
    "currentfeed - prints url of current feed",
    "search 'keyword' - search for a keyword in the current feed",
    "searchtitle 'keyword' - same as search but searches only in title",
    "searchdesc 'keyword' - same as search but searches only in description"
]
feeds = ["mostRead", "majorHeadlines", "recent", "sports"]
url = ""
currentFeed = ""
version = "4.2-1"

print("Enter help to get list of all command")

loop = True
while loop:
    if currentFeed == "majorHeadlines":
        url = "https://feeds.yle.fi/uutiset/v1/majorHeadlines/YLE_UUTISET.rss"
    if currentFeed == "mostRead":
        url = "https://feeds.yle.fi/uutiset/v1/mostRead/YLE_UUTISET.rss"
    if currentFeed == "recent":
        url = "https://feeds.yle.fi/uutiset/v1/recent.rss?publisherIds=YLE_UUTISET"
    if currentFeed == "sports":
        url = "https://feeds.yle.fi/uutiset/v1/majorHeadlines/YLE_URHEILU.rss"

    if currentFeed == "":
        userPrompt = input("<> ")
    else:
        userPrompt = input("<{}> ".format(currentFeed))

    if userPrompt == "help":
        for command in commands:
            print(command)

    if userPrompt == "exit" or userPrompt == "quit":
        loop = False

    if userPrompt == "titles" or userPrompt == "ls" and currentFeed != "":
        feed = feedparser.parse(url)
        articles = feed['entries']
        length = len(articles)

        for i in range(length):
            print("{}. {}".format(i, articles[i]['title']))
    elif userPrompt == "titles" or userPrompt == "ls" and currentFeed == "":
        print("No feed selected. Select feed using setfeed command")

    if userPrompt.startswith("article") or userPrompt.startswith("get") and currentFeed != "":
        if userPrompt.startswith("article"):
            articleIndex = int(userPrompt.replace("article ", ""))
        elif userPrompt.startswith("get"):
            articleIndex = int(userPrompt.replace("get", ""))
        feed = feedparser.parse(url)
        article = feed['entries'][articleIndex]

        title = article['title']
        desc = article['description']
        link = article['link'].replace("?origin=rss", "")
        RSSlink = article['link']
        date = article['published']

        print("Title: " + title)
        print("Published: " + date)
        print("Link: " + link)
        print("RSSLink: " + RSSlink)
        print("Description: " + desc)
    elif userPrompt.startswith("article") or userPrompt.startswith("get") and currentFeed == "":
        print("No feed selected. Select feed using setfeed command")

    if userPrompt.startswith("setfeed"):
        queryFeed = userPrompt.replace("setfeed ", "")
        found = False

        for feed in feeds:
            if feed == queryFeed:
                found = True

        if not found:
            try:
                queryIndex = int(queryFeed)
                if queryIndex >= 0 and queryIndex < len(feeds):
                    feed = feeds[queryIndex]
                    queryFeed = feeds[queryIndex]
                    found = True
            except ValueError:
                found = False

        if found == True:
            currentFeed = queryFeed
        else:
            print("Feed not found")
            print("Try feeds command that lists all feeds")

    if userPrompt == "feeds":
        for feed in feeds:
            print(feed)

    if userPrompt == "clear" or userPrompt == "c":
        os.system("clear")

    if userPrompt == "currentfeed" and currentFeed != "":
        print(url)
    elif userPrompt == "currentfeed" and currentFeed == "":
        print("No feed selected. Select feed using setfeed command")

    if userPrompt.startswith("search") and currentFeed != "":
        title = False
        desc = False
        search = ""
        if userPrompt.startswith("search "):
            title = True
            desc = True
            search = userPrompt.replace("search ", "")
        elif userPrompt.startswith("searchtitle "):
            title = True
            desc = False
            search = userPrompt.replace("searchtitle ", "")
        elif userPrompt.startswith("searchdesc "):
            title = False
            desc = True
            search = userPrompt.replace("searchdesc ", "")
        feed = feedparser.parse(url)
        articles = feed['entries']
        list = []
        length = len(articles)

        for i in range(length):
            found = False
            if search in articles[i]['title'] and title:
                found = True
            if search in articles[i]['description'] and desc:
                found = True

            if found == True:
                list.append("{}. {}".format(i, articles[i]['title']))

        for item in list:
            print(item)
    elif userPrompt.startswith("search") and currentFeed == "":
         print("No feed selected. Select feed using setfeed command")

    if userPrompt == "version":
        print(version)
