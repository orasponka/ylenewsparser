# Yle News Parser
Yle News Parser is tool that read news from feeds.yle.fi <br>
At the moment Yle News Parser package is officially available only for Arch Linux and Arch Linux based distros. <br>
Works on other Linux distros but i don't know about Windows. <br>
Developed and tested on Arch Linux. 

Yle News Parser is not official Yle product and its not connected to Yle. <br>
Yle News Parser is licensed under MIT License. 

## How to run Yle News Parser?
Do things below.
```
git clone https://gitlab.com/orasponka/ylenewsparser.git
cd ylenewsparser/
pip install feedparser
python main.py
```
## How to install Yle News Parser?
### Arch Linux
If you have enabled my [op-arch-repo](https://gitlab.com/orasponka/op-arch-repo) you can use pacman.
```
sudo pacman -Sy ylenewsparser
```
If you do not, do things below.
```
git clone https://gitlab.com/orasponka/ylenewsparser.git
cd ylenewsparser/
makepkg
sudo pacman -U *.zst
```
